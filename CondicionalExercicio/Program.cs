﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CondicionalExercicio
{
    class Program
    {
        static void Main(string[] args)
        {
            int a, b;

            Console.Write("Digite o valor de A:");
            a = int.Parse(Console.ReadLine());

            Console.Write("Digite o valor de B:");
            b = int.Parse(Console.ReadLine());

            if (a > b)
            {
                Console.Write("B: " + b + " - A: "+a);
            }
            else
            {
                Console.Write("A: " + a + " - B: " + b);
            }

            Console.ReadKey();
        }
    }
}
